## Install

Change the release-1.0.0 below to the latest release version:

```bash
npm install gitlab:oyoclass/oyoapp-clientlib-nodejs
```


## Usage

```javascript
var base_url = "http://oyoclass.com:8081/api/",
    client_id = "<your app id>",
    secret = "<your app secret>";

var oyoapp = require("oyoapp-clientlib-nodejs");
var client = new oyoapp.APIClient(client_id, secret, base_url);

client.issueAppBadge(...);
```

For more example, check ```test.js```


## Test

Open ```test.js```, change the ```base_url```, ```client_id``` and ```secret``` as you want, then uncomment the API function you want to test, finally run:

```bash
npm test
```
