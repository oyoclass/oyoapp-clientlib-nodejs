(function () {
    'use strict';

    var oyoapp = require("./oyoapp.js");

    // change the base_url, client_id, secret, uid as you want
    var base_url = "http://codecoaching.org:8080/api/",
        client_id = "56bfa5a87333f7222f53be9a",
        secret = "693ae178ba4c37b669e425c280b848df9a101d68",
        uid = "564511377333f736ed258d1b";

    var client = new oyoapp.APIClient(client_id, secret, base_url);

    /*
    console.log("*** Testing getUserInfo ...");
    client.getUserInfo(uid, function (err, status, body) {
        if (err) {
            console.log("Error: ", err);
            return;
        }
        // status's type if number, if status === 200
        console.log("Status: " + status);
        console.log("Body:\n" + JSON.stringify(body, null, 4));
    });
    */

    /*
    console.log("*** Testing getUsersInfo ...");
    let uids = ["564511377333f736ed258d1b", "56d4d7137333f710dba754d1"];
    client.getUsersInfo(uids, function (err, status, body) {
        if (err) {
            console.log("Error: ", err);
            return;
        }
        // status's type if number, if status === 200
        console.log("Status: " + status);
        console.log("Body:\n" + JSON.stringify(body, null, 4));
    });
    */

    /*
    console.log("*** Testing postUserPortfolio ...");
    client.postUserPortfolio(uid, "python", "Creeper Head", 
        "https://pythonmini.oyohub.com/file/55a7ce6cf3c512306acfa841", true,
        function (err, status, body) {
           if (err) {
               console.log("Error: ", err);
               return;
           }
           console.log("Status: " + status);
           console.log("Body:\n" + JSON.stringify(body, null, 4));
        }
    );
    */

    /*
    console.log("*** Testing issueAppBadge ...");
    client.issueAppBadge(uid, "56c28f907333f72d781385c9", 
        "564511377333f736ed258d21", "", 
        function (err, status, body) {
            if (err) {
            console.log("Error: ", err);
            return;
            }   
            console.log("Status: " + status);
            console.log("Body:\n" + JSON.stringify(body, null, 4));
        }   
    );  
    */

}());
