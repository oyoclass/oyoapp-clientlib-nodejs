/*global require, Buffer, module, querystring*/
(function() {
    "use strict";

    var crypto = require("crypto");
    var http = require("http");
    var https = require("https");
    var url = require("url");
    var querystring = require("querystring");

    function createHmacSig(secret, msg) {
        var hmac = crypto.createHmac("md5", secret);
        hmac.update(msg);
        return hmac.digest('hex');
    }

    var verifySignedReq = function(signedReq, secret, checkExpireTime, callback) {
        if(!signedReq) {
            callback("Invalid format for signedReq");
            return;
        }
        var sigMsg = signedReq.split(".");
        if(sigMsg.length !== 2) {
            callback("Invalid format for signedReq");
            return;
        }
        if(checkExpireTime !== false) {
            checkExpireTime = true;
        }
        
        var sig = sigMsg[0];
        var msg = sigMsg[1];
        
        if(createHmacSig(secret, msg) !== sig) {
            callback("Invalid signature");
            return;
        }
        var decodeMsg = null;
        try {
            decodeMsg = JSON.parse((new Buffer(msg, "base64")).toString());
            var expire = decodeMsg.expire_time;
            var now = Date.now() / 1000;
            if(checkExpireTime && now > expire) {
                callback("Invalid request");
                return;
            }
        } catch(err) {
            callback("Invalid req info for decode/json");
            return;
        }
        callback(null, decodeMsg);
    };

    var genReqSignature = function(args, secret) {
        var keys = Object.keys(args).sort();
        var vals = [];
        keys.forEach(function(key) {
            vals.push(String(args[key]));
        });
        return createHmacSig(secret, vals.join(""));
    };

    function sendReq(reqUrl, args, timeout, method, callback) {
        var callbackCalled = false;
        var callbackOnce = function() {
            if(!callbackCalled) {
                callbackCalled = true;
                if(callback) {
                    callback.apply(this, arguments);
                }
            }
        };
        if(!callback) {
            if(typeof method === "function") {
                callback = method;
                method = null;
            } else {
                callback = function() {};
            }
        }
        if(!method) {
            method = "GET";
        }
        if(typeof reqUrl === "string") {
            reqUrl = url.parse(reqUrl);
        }
        if(!args) {
            args = {};
        }
        var data = querystring.stringify(args);
        if(method === "GET" && data.length > 0) {
            reqUrl.path = reqUrl.path + "?" + data;
        }
        var reqOptions = {
            host: reqUrl.host,
            hostname: reqUrl.hostname,
            port: reqUrl.port,
            path: reqUrl.path,
            auth: reqUrl.auth,
            method: method
        };

        if (method === "POST") {
            reqOptions["headers"] = {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': Buffer.byteLength(data)
            }
        }
        
        var protocol = http;
        if(reqUrl.protocol && reqUrl.protocol.toLowerCase() === "https:") {
            protocol = https;
        }

        var req = protocol.request(reqOptions, function(res) {
            var resBody = [];
            res.on("data", function(chunk) {
                resBody.push(chunk);
            });
            res.on("error", function(err) {
                callbackOnce(err);
            });
            res.on("end", function() {
                resBody = resBody.join("");
                callbackOnce(null, res.statusCode, resBody);
            });
        });
        req.setTimeout(timeout, function() {
            callbackOnce(null, 11, "Socket timeout");
            req.abort();
        });
        req.on("error", function(err) {
            callbackOnce(err);
        });
        if(method === "POST") {
            req.write(data);
        }
        req.end();
    }

    var APIClient = function(clientId, secret, baseUrl, timeout) {
        var self = this;
        if(!baseUrl) {
            baseUrl = "https://oyoclass.com/api/";
        }
        if(isNaN(timeout)) {
            timeout = 5000;
        } else {
            timeout = Number(timeout);
        }
        this.clientId = clientId;
        this.secret = secret;
        if(!baseUrl.endsWith("/")) {
            baseUrl += "/";
        }
        this.baseUrl = baseUrl;
        this.timeout = timeout;
        this.apiUrls = {
            "user.json": "user.json",
            "users.json": "users.json",
            "user-use-app.json": "user-use-app.json",
            "user/portfolio": "user/portfolio",
            "user/app-badge": "user/app-badge"
        };

        Object.keys(this.apiUrls).forEach(function(key) {
            self.apiUrls[key] = url.resolve(self.baseUrl, self.apiUrls[key]);
        });
    };

    APIClient.prototype._completeReqArgs = function(args) {
        args.ts = String(Date.now());
        args.client_id = this.clientId;
        args.signature = genReqSignature(args, this.secret);
        return args;
    };

    APIClient.prototype.getUserInfo = function(uid, callback) {
        var reqUrl = this.apiUrls["user.json"];
        var args = {
            uid: uid
        };
        args = this._completeReqArgs(args);
        sendReq(reqUrl, args, this.timeout, function(err, statusCode, resBody) {
            if(err) {
                callback(err);
                return;
            }

            if(statusCode !== 200) {
                callback(null, statusCode, {});
                return;
            }
            try {
                resBody = JSON.parse(resBody);
            } catch(e) {
                resBody = {};
            }
            callback(null, statusCode, resBody);
        });
    };

    APIClient.prototype.getUsersInfo = function(uids, callback) {
        var reqUrl = this.apiUrls["users.json"];
        var args = {
            uids: uids.map((uid) => uid.toString()).join(",")
        };
        args = this._completeReqArgs(args);
        sendReq(reqUrl, args, this.timeout, function(err, statusCode, resBody) {
            if(err) {
                callback(err);
                return;
            }

            if(statusCode !== 200) {
                callback(null, statusCode, {});
                return;
            }
            try {
                resBody = JSON.parse(resBody);
            } catch(e) {
                resBody = {};
            }
            callback(null, statusCode, resBody);
        });
    };

    APIClient.prototype.postUserUseApp = function(uid, callback) {
        var reqUrl = this.apiUrls["user-use-app.json"];
        var args = {
            uid: uid
        };
        args = this._completeReqArgs(args);
        sendReq(reqUrl, args, this.timeout, "POST", callback);
    };

    APIClient.prototype.postUserPortfolio = function(uid, category, itemName, itemLink, itemShow, callback) {
        var reqUrl = this.apiUrls["user/portfolio"];
        var args = {
            uid: uid,
            category: category,
            item_name: itemName,
            item_link: itemLink,
            item_show: itemShow ? "1" : "0"
        };
        args = this._completeReqArgs(args);
        sendReq(reqUrl, args, this.timeout, "POST", callback);
    };

    APIClient.prototype.issueAppBadge = function(uid, badgeid, communityid, note, callback) {
        var reqUrl = this.apiUrls["user/app-badge"];
        var args = {
            uid: uid,
            badgeid: badgeid
        };
        if(communityid) {
            args.communityid = communityid;
        }
        if(note) {
            args.note = note;
        }
        args = this._completeReqArgs(args);
        sendReq(reqUrl, args, this.timeout, "POST", callback);
    };

    module.exports = {
        APIClient: APIClient,
        genReqSignature: genReqSignature,
        verifySignedReq: verifySignedReq
    };
})();
